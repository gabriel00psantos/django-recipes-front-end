## Django-recipes-frontend

Uma aplicação que cadastra e deleta informações relacionadas
a "receitas", entidade essa que tem relacionamento "um para muitos" com outra entidade basica de "comentarios" sobre essas receitas. Os usuarios podem cadastrar ambas e só podem deletar seus proprios comentarios e receitas, que serão listadas para todos. Construida usando React, Redux, JavaScript, and CSS (materializecss).

## Status do projeto

Foi um "remember" de alguns topicos de Django alem de um hands-on com django-rest & JWT integrados com o front-end 

## Instalação

Clone esse repositório. Será necessario ter o `node` e o `npm` instalados globalmente em sua maquina.  

Instalação:

`npm install`  

To Start Server:

`npm start`  

To Visit App:

`localhost:3000/`  

## Reflexão

Projeto desenvolvido com o intuito de exercitar minhas habilidades. :D

![alt text](https://i.kym-cdn.com/entries/icons/mobile/000/028/021/work.jpg)

