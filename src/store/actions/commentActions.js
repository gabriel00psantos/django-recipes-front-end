import axios from 'axios';
import toaster from "toasted-notes";

const api_endpoint = "http://localhost:8000/";

export const createComment = (comment) => {
	return (dispatch, getState) => {
		if (!comment){
				toaster.notify("Please, fill in the field below :o", {
					duration: 4000
				});
				return;
			}
		
		const { tokenAuth } = getState().auth;
		
		const config = {
    		headers: { Authorization: `JWT ${tokenAuth}` }
		};

		axios.post(`${ api_endpoint}recipes/${ comment.recipe_id }/comments/`, comment, config).then(response => {
			if (response.status !== 201){
				toaster.notify("Comment not Inserted :o", {
					duration: 4000
				});
				dispatch({ type: 'MAKE_REQUEST_ERROR'}, {error: 'error'});
			}else {
				toaster.notify("Comment Inserted :D", {
					duration: 4000
				});
				dispatch({ type: 'CREATE_COMMENT'});
				dispatch(getComments(comment.recipe_id));
			}
		}).catch(err => {
			toaster.notify("There's something wrong :(", {
					duration: 4000
				});
			dispatch({ type: 'MAKE_REQUEST_ERROR', err});
		})
	}

}

export const getComments = (recipe_id) => {
	return (dispatch, getState) => {
		const { tokenAuth } = getState().auth;
		
		const data = {
		}

		const config = {
    		headers: { Authorization: `JWT ${tokenAuth}` }
		};
		dispatch({ type: 'MAKE_REQUEST'});

		axios.get(`${ api_endpoint}recipes/${ recipe_id }/comments/`, data, config).then(response => {
			if (response.status !== 200){
				toaster.notify("There's something wrong :O", {
					duration: 4000
				});
				dispatch({ type: 'MAKE_REQUEST_ERROR'}, {error: 'error'});
			}else {
				dispatch({type: 'MAKE_REQUEST_SUCCESS'});
				dispatch({ type: 'COMMENT_LIST', 'comment_list': response.data.results});
			}
		}).catch(err => {
			toaster.notify("There's something wrong :(", {
					duration: 4000
				});
			dispatch({ type: 'MAKE_REQUEST_ERROR'}, {error: 'error'});
		})
	}
}

export const deleteComment = (comment) => {
	return (dispatch, getState) => {
		const { tokenAuth } = getState().auth;
		
		const config = {
    		headers: { Authorization: `JWT ${tokenAuth}` }
		};
		dispatch({ type: 'MAKE_REQUEST'});

		axios.delete(`${ api_endpoint}comments/${ comment.id }/`, config, {}).then(response => {
			if (response.status !== 204){
				toaster.notify("There's something wrong :O", {
					duration: 4000
				});
				dispatch({ type: 'MAKE_REQUEST_ERROR'}, {error: 'error'});
			}else {
				dispatch(getComments(comment.recipe_id));
				toaster.notify("Comment Deleted ;D", {
					duration: 4000
				});
				
				dispatch({type: 'MAKE_REQUEST_SUCCESS'});
			
			}
		}).catch(err => {
			toaster.notify("There's something wrong :(", {
					duration: 4000
				});
			dispatch({ type: 'MAKE_REQUEST_ERROR'}, {error: 'error'});
		})
	}
}