import axios from 'axios';
import toaster from "toasted-notes";

const api_endpoint = "http://localhost:8000/";

export const createRecipe = (recipe) => {
	return (dispatch, getState) => {
		if (!recipe.recipe_title || !recipe.recipe_content || !recipe.recipe_thumb_url){
				toaster.notify("Please, fill in the fields below :)", {
					duration: 2000
				});
				return;
		}

		const { tokenAuth } = getState().auth;
		
		const config = {
    		headers: { Authorization: `JWT ${tokenAuth}` }
		};
		dispatch({ type: 'MAKE_SEARCH'});

		axios.post(`${ api_endpoint}recipes/`, recipe, config).then(response => {
			if (response.status !== 201){
				dispatch({ type: 'MAKE_REQUEST_ERROR'}, {error: 'error'});
				toaster.notify("Recipe not Inserted :(", {
					duration: 4000
				});
			}else {
				dispatch({ type: 'MAKE_REQUEST_SUCCESS'});
				upLoadFile(recipe);
				toaster.notify("Recipe Inserted", {
					duration: 4000
				});
			}
		}).catch(err => {
			dispatch({ type: 'MAKE_REQUEST_ERROR', err});
			toaster.notify("There's something wrong", {
					duration: 2000
				});
		})
	}

}

const upLoadFile = (obj) => {
	const formData = new FormData(); 
     
      formData.append( 
        "myimage", 
        obj.file, 
        obj.file.name 
      );

      const config = {
	        headers: {
	            'content-type': 'multipart/form-data'
	        }
    	}
		axios.post(`${ api_endpoint}upload-files/`, formData, config).then(response => {
			if (response.status === 200){
				console.log("Nice");
			}else {
				console.log("Hummm");
			}
		}).catch(err => {
			
		});
}


export const listRecipes = (recipe) => {
	return (dispatch, getState) => {
		const { tokenAuth } = getState().auth;
		
		const data = {
		}

		const config = {
    		headers: { Authorization: `JWT ${tokenAuth}` }
		};
		dispatch({ type: 'MAKE_REQUEST'});

		axios.get(`${ api_endpoint}recipes/`, data, config).then(response => {
			
			
			if (response.status !== 200){
				dispatch({ type: 'MAKE_REQUEST_ERROR'}, {error: 'error'});
			}else {
				dispatch({type: 'MAKE_REQUEST_SUCCESS'});
				dispatch({ type: 'RECIPE_LIST', 'recipes': response.data.results}, );
			}
		}).catch(err => {
			dispatch({ type: 'MAKE_REQUEST_ERROR'}, {error: 'error'});
		})
	}
}

export const getRecipe = (recipe_id) => {
	return (dispatch, getState) => {
		const { tokenAuth } = getState().auth;
		
		const data = {
		}

		const config = {
    		headers: { Authorization: `JWT ${tokenAuth}` }
		};
		dispatch({ type: 'MAKE_REQUEST'});

		axios.get(`${ api_endpoint}recipes/${ recipe_id }/`, data, config).then(response => {
			
			
			if (response.status !== 200){
				dispatch({ type: 'MAKE_REQUEST_ERROR'}, {error: 'error'});
			}else {
				dispatch({type: 'MAKE_REQUEST_SUCCESS'});
				dispatch({ type: 'RECIPE_LIST_DETAIL', 'recipe': response.data});
			}
		}).catch(err => {
			dispatch({ type: 'MAKE_REQUEST_ERROR'}, {error: 'error'});
		})
	}
}


export const deleteRecipe = (recipe_id) => {
	return (dispatch, getState) => {
		const { tokenAuth } = getState().auth;
		const config = {
    		headers: { Authorization: `JWT ${tokenAuth}` }
		};
		dispatch({ type: 'MAKE_REQUEST'});

		axios.delete(`${ api_endpoint}recipes/${ recipe_id }/`, config, {}).then(response => {
			
			
			if (response.status !== 204){
				toaster.notify("Error on Delete Recipe :D", {
					duration: 4000
				});
				dispatch({ type: 'MAKE_REQUEST_ERROR'}, {error: 'error'});
			}else {
				toaster.notify("Recipe Deleted :D", {
					duration: 4000
				});
				dispatch(listRecipes());
				dispatch({type: 'MAKE_REQUEST_SUCCESS'});
				dispatch({ type: 'RECIPE_LIST_DETAIL', 'recipe': response.data});
			}
		}).catch(err => {
			toaster.notify("There's something wrong! Try again later :D", {
					duration: 4000
				});
			dispatch({ type: 'MAKE_REQUEST_ERROR'}, {error: 'error'});
		})
	}
}