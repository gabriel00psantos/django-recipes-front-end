import axios from 'axios';
const api_endpoint = 'http://localhost:8000/auth/';


export const login = (user) => {
	return (dispatch, getState) => {
		axios.post(`${api_endpoint}token/obtain/`, user).then((response) =>{
			let tokenAuth = response.data.access;
			let user = {
				username: response.data.username,
				id: response.data.id,
				email: response.data.email
			}
			dispatch({type: 'LOGIN_SUCCESS', tokenAuth, user})
		}).catch((err) => {
			console.log(err);
			dispatch({type: 'LOGIN_ERROR'})
		}).then(()=> {
			console.log('!!!')
		})
	}
}

export const signUp = (user) => {
	return (dispatch, getState) => {
		axios.post(`${api_endpoint}user/create/`, user).then((response) =>{
			console.log(response);
		}).catch((err) => {
			console.log(err);
		}).then(()=> {
			console.log('!!!!')
		})
	}
}

export const logOut = () => {
	return (dispatch, getState) => {
		console.log('logout')
		dispatch({ type: 'LOGOUT'})
	}
}