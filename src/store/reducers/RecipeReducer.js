const initialState = {
	sending_request: false,
	total_recipes: 0,
	recipe_detail: {},
	recipe_list: [
	],
	error: ''
}


const RecipeReducer = (state=initialState, action) => {
	switch(action.type){
		case 'MAKE_REQUEST':
			return {
				...state,
				sending_request: true,
			}

		case 'MAKE_REQUEST_SUCCESS':
			return {
				...state,
				sending_request: false,
			}
			
		case 'MAKE_REQUEST_ERROR':
			return {
				...state,
				error: action.error,
				sending_request: false,
			}
		case 'CREATE_RECIPE':
			return {
				...state,
				sending_request: false
		}
		case 'RECIPE_LIST':
			return {
				...state,
				sending_request: false,
				recipe_list: [ ...action.recipes]
		}
		case 'RECIPE_LIST_DETAIL':
			return {
				...state,
				sending_request: false,
				recipe_detail: action.recipe
		}
		default:
			return state
	}

}

export default RecipeReducer;