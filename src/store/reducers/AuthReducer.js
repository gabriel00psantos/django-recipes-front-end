const initialState = {
	isAuth: false,
	user: null,
	tokenAuth: null
}

const AuthReducer = (state=initialState, action) => {
	switch(action.type){
		case 'LOGIN_ERROR':
			return {
				...state,
				user: null,
				tokenAuth: null,
				isAuth: false
		}
		case 'LOGIN_SUCCESS':
			return {
				...state,
				user: action.user,
				tokenAuth: action.tokenAuth,
				isAuth: true
		}
		case 'SIGNUP_ERROR':
			return {
				...state,
		}
		case 'SIGNUP_SUCCESS':
			return {
				...state,
		}
		case 'LOGOUT':
			return {
				...state,
				user: null,
				tokenAuth: null,
				isAuth: false,
		}
		default:
			return {
				...state
			}
	}
}

export default AuthReducer