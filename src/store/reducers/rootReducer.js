import authReducer from './AuthReducer';
import RecipeReducer from './RecipeReducer';
import CommentReducer from './CommentReducer';

import { combineReducers } from 'redux';


const rootReducer = combineReducers({
	auth: authReducer,
	recipe: RecipeReducer,
	comment: CommentReducer
});

export default rootReducer;