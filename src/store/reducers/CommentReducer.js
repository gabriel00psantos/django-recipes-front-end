const initialState = {
	sending_request: false,
	total_comments: 0,
	comment_detail: {},
	comment_list: [
	],
	error: ''
}


const RecipeReducer = (state=initialState, action) => {
	switch(action.type){
		case 'CREATE_COMMENT':
			return {
				...state,
				sending_request: false
		}
		case 'COMMENT_LIST':
			return {
				...state,
				sending_request: false,
				comment_list: [ ...action.comment_list]
		}
		default:
			return state
	}

}

export default RecipeReducer;