import React, { Component } from 'react';
import { connect } from 'react-redux';
import { listRecipes, deleteRecipe } from '../../store/actions/recipeActions';
import Loader from '../layouts/Loader';
import ListRecipe from '../recipes/ListRecipe';

class Dashboard extends Component{
	componentDidMount(){
		this.props.listaAll();
	}

	render(){
		const { recipeList } = this.props;
		const { user } = this.props;
		const { delRecipe } = this.props;
		return (
			<div className="container parent">
				<div className="box child">
					<div className="row">
						<h3 className="center-align">Good Recipes</h3>
						<div className="center-align">
							<ListRecipe 
								delRecipe={ delRecipe } 
								user={ user } 
								recipeList={ recipeList }
							/>
						</div>
					</div>
				</div>
				<Loader active={ this.props.sending_request }/>
			</div>
		)
	}
}

const mapStateToProps = (state) => {
	return {
		user: state.auth.user,
		isAuth: state.auth.isAuth,
		recipeList: state.recipe.recipe_list,
		sending_request: state.recipe.sending_request,
	}
}

const mapDispatchToProps = (dispatch) =>{
	return {
		listaAll: () => dispatch(listRecipes()),
		delRecipe: (recipe_id) => dispatch(deleteRecipe(recipe_id))
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);