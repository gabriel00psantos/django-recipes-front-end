import React, { Component } from 'react';
import { connect } from 'react-redux';
import { createComment } from '../../store/actions/commentActions';

class CreateComment extends Component{
	state = {
		comment_content: '',
		recipe_id: this.props.recipe_id
	}

	constructor(props){
		super(props);
		this.fileInput = React.createRef();
	}

	handleChange = (e) => {
		this.setState({
			[e.target.id]: e.target.value
		})
	}
	handleSubmit = (e) => {
		e.preventDefault();
		e.stopPropagation();
		e.nativeEvent.stopImmediatePropagation();
		this.props.makeComment(this.state);
		this.setState({
			comment_content: ''
		});
	}

	render(props){
		return (
			<div className="row">
					    <form className="col s12">
			      			<div className="row">
						        <div className="input-field col s12">
						          <input placeholder="" 
						          		 id="comment_content" 
						          		 value={this.state.comment_content}
						          		 type="text" className="validate"
						          		 onChange={this.handleChange} />
						          <label htmlFor="comment_content">Add Comment</label>
						        </div>
						    </div>
						    <div className="row">
					    	    <div className="input-field col s12">
					    	    	<button onClick={ this.handleSubmit } className="waves-effect waves-light btn">Send Comment</button>
						    	</div>
						    </div>
				        </form>
					</div>
	    )
	}
}

const mapStateToProps = (state) => {
	return {
		sending_request: state.comment.sending_request,
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
		makeComment: (recipe) => dispatch(createComment(recipe))
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateComment);