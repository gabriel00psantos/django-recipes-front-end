import React from 'react'
import moment  from 'moment';

const ListSearch = (props) =>{
	return (
		<div className="row">
			{
				props.comments && props.comments.map(comment => {
					let create_time = moment(comment.comment_create_time).format( "DD/MM/YYYY hh:mm:ss")
					return (
						<li class="collection-item avatar">
							<div>
								<img src="images/yuna.jpg" alt="" class="circle"/>
								<span class="title">"{comment.comment_content}" </span>
								<p>Made By : { comment.owner_name }<br/>
								{ create_time } 
								</p>
								{
									props.user && (props.user.id === comment.owner_id) && 

										<a href="#!" onClick={ () => props.del(comment) } class="secondary-content">
											<i class="material-icons">cancel</i>
										</a>
								}
							</div>
						</li>
					)
				}) 
			}
		</div>
	)
}

export default ListSearch;





