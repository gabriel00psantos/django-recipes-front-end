import React from 'react'
import CommentDetails from './CommentDetails';

const ListComments = (props) =>{
	return (
		<div className="row">
			<ul className="collection">
					<li className="collection-header"><h4>Comments</h4></li>
					<CommentDetails del={ props.del } user={ props.user } comments={props.comments}/>
			</ul>
		</div>
	)
}

export default ListComments;