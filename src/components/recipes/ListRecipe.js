import React from 'react'
import  { Link } from 'react-router-dom';

const isImage = require('is-image');

const ListRecipe = (props) =>{

const delete_recipe = (recipe, e) => { 
	e.preventDefault();
	props.delRecipe(recipe.id);
}

return (
		<div className="row">
			{
				props.recipeList && props.recipeList.map(recipe => {
					let is_image = isImage("http://localhost:8000/mediafiles/" + recipe.recipe_thumb_url)
					let photo_url = is_image ? "http://localhost:8000/mediafiles/" + recipe.recipe_thumb_url  : "https://bitsofco.de/content/images/2018/12/Screenshot-2018-12-16-at-21.06.29.png"
					return (
						<Link key={recipe.id} to={ /details/ + recipe.id }>
							<div className="col s12 m3 hoverable">
									<div className="card big">
										<div className="card-image">
											<img alt="" className="list-img-dashboard" src= { photo_url }  />
											{ props.user && (recipe.owner_id === props.user.id) &&
												<a 
													href="#!" 
													className="btn-floating halfway-fab waves-effect waves-light red"
													onClick={ delete_recipe.bind(this, recipe) }
												>
												<i className="material-icons">cancel</i></a>
											}
										</div>
									<div className="card-content dashboard-list">
										<span className="card-title card-dash">{ recipe.recipe_title }</span>
										
										{ props.user && (recipe.owner_id === props.user.id) &&
											<span className="col s12"><i class="tiny material-icons">person</i>By you :D</span>
										}
										{ !props.user &&
											<span className="col s12"><i class="tiny material-icons">person</i>{ recipe.owner_name }</span>
										}

									</div>
								</div>
							</div>
						</Link>
					)
				}) 
			}
		</div>
	)
}

export default ListRecipe;