import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { createRecipe } from '../../store/actions/recipeActions';
import Loader from '../layouts/Loader';

class CreateRecipe extends Component{
	state = {
		recipe_title: '',
		recipe_content: '',
		recipe_thumb_url: ''

	}

	constructor(props){
		super(props);
		this.fileInput = React.createRef();
	}

	handleChange = (e) => {
		this.setState({
			[e.target.id]: e.target.value
		})
	}

	handleFileChange = (e) => {
		this.setState({
			'recipe_thumb_url': this.fileInput.current.files[0].name,
			'file': this.fileInput.current.files[0]
		})
	}

	handleSubmit = (e) => {
		e.preventDefault();
		e.stopPropagation();
		e.nativeEvent.stopImmediatePropagation();
		this.props.makeSearch(this.state);

		this.setState({
			recipe_title: '',
			recipe_content: '',
			recipe_thumb_url: ''
		});
	}

	render(props){
		let { isAuth } = this.props ;
		
		if (!isAuth) return <Redirect to='/signin'/>
		return (
			<div className="container">
				<div className="box">
					<div className="row">
					    <h3 className="center-align">Register your recipe <span alt="" aria-label="" role="img">&#128512;</span></h3>
					    <form className="col s12">
			      			<p>Share your recipe with your friends</p>
					    	<div className="row">
						        <div className="input-field col s6">
						          <input placeholder="" 
						          		 id="recipe_title"
						          		 value={ this.state.recipe_title }
						          		 type="text" className="validate"
						          		 onChange={this.handleChange} />
						          <label htmlFor="recipe_title">Recipe Title</label>
						        </div>
						        <div className="input-field col s6">
									<div className="file-field input-field">
										<div className="btn">
											<span>Recipe image</span>
											<input onChange={this.handleFileChange} type="file" ref={this.fileInput}/>
										</div>
										<div className="file-path-wrapper">
											<input className="file-path validate" type="text" />
										</div>
									</div>
								</div>
						        <div className="input-field col s12">
						          <input placeholder="" 
						          		 id="recipe_content" 
						          		 value={ this.state.recipe_content }
						          		 type="text" className="validate"
						          		 onChange={this.handleChange} />
						          <label htmlFor="recipe_content">Recipe Content</label>
						        </div>
						    </div>
						    <div className="row">
					    	    <div className="input-field col s12">
					    	    	<button onClick={ this.handleSubmit } className="waves-effect waves-light btn">Send recipe</button>
						    	</div>
						    </div>
				        </form>
						{/*<div className="col s6 right">
							<div className="row">
		        				<div className="col s12">
		        					<ListSearch searches={ this.props.list }/>	
		        				</div>
	        				</div>
	        			</div>*/}
	        		</div>
	        	</div>
	        	<Loader active={ this.props.sending_request }/>
			</div>
		)
	}
}

const mapStateToProps = (state) => {
	return {
		isAuth: state.auth.isAuth,
		sending_request: state.recipe.sending_request,
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
		makeSearch: (recipe) => dispatch(createRecipe(recipe))
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateRecipe);