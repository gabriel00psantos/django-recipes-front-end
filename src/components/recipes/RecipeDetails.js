import React, { Component } from 'react';
import { connect } from 'react-redux';
import Loader from '../layouts/Loader';
import { getRecipe } from '../../store/actions/recipeActions';
import { getComments, deleteComment } from '../../store/actions/commentActions';
import  { Link } from 'react-router-dom';
import ListComments from '../comments/ListComment';
import CreateComment from '../comments/CreateComment';


class RecipeDetails extends Component{
	componentDidMount(){
		console.log(this.props.recipe_id);
		this.props.getRecipeDetails(this.props.recipe_id);
		this.props.getRecipeComments(this.props.recipe_id);
	}

	render(){
		const { recipe_object } = this.props;
		const { recipe_comments } = this.props;
		const { recipe_id } = this.props;
		const { isAuth } = this.props;
		const { user } = this.props;
		const isImage = require('is-image');
		let is_image = isImage("http://localhost:8000/mediafiles/" + recipe_object.recipe_thumb_url)
		let photo_url = is_image ? "http://localhost:8000/mediafiles/" + 
					 	recipe_object.recipe_thumb_url  : 
					 	"https://bitsofco.de/content/images/2018/12/Screenshot-2018-12-16-at-21.06.29.png"
		return (
			<div className="container">
				<div className="box">
					<div className="row">
						<div className="center-align">
							<div className="row">
								<div className="col s12 m12">
									<div className="card">
										<div className="card-image">
											<img alt="" src={ photo_url }/>
											<span className="card-title"></span>
										</div>
										<div className="card-content">
											<h3>{ recipe_object.recipe_title }</h3>
											<p>{ recipe_object.recipe_content }</p>

											<div className="row margin-top-50">
												<span className="col s6 left'"><i class="material-icons">person</i> { recipe_object.owner_name }</span>
												<span className="col s6 right"><i class="material-icons">comment</i> { recipe_object.total_comments}</span>
											</div>
										</div>
										<div className="card-action">
											<Link to="/">Back to Dashboard</Link>
										</div>
									</div>
								</div>
							</div>
							<ListComments del={ this.props.delComment } 
										  user={ user } 
										  isAuth={ isAuth } 
										  recipe_id={ recipe_id } 
										  comments={ recipe_comments }
							/>
							{
								isAuth &&
									<CreateComment recipe_id={ recipe_id }/>
							}

						</div>
					</div>
				</div>
				<Loader active={ this.props.sending_request }/>
			</div>
		)
	}
}

const mapStateToProps = (state, ownProps) => {
	const recipe_id = ownProps.match.params.id;
	
	return {
		user: state.auth.user,
		recipe_id: recipe_id,
		isAuth: state.auth.isAuth,
		sending_request: state.recipe.sending_request,
		recipe_object: state.recipe.recipe_detail,
		recipe_comments: state.comment.comment_list
	}
}

const mapDispatchToProps = (dispatch) =>{
	return {
		getRecipeDetails: (recipe_id) => dispatch(getRecipe(recipe_id)),
		getRecipeComments: (recipe_id) => dispatch(getComments(recipe_id)),
		delComment: (comment) => dispatch(deleteComment(comment))
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(RecipeDetails);