import React, { Component } from 'react';
import { connect } from 'react-redux';
import { login } from '../../store/actions/authActions';
import { Redirect } from 'react-router-dom';
 
class Signin extends Component{
	state = {
		email: '',
		username: '',
		password: ''
	}

	handleChange = (e) => {
		this.setState({
			[e.target.id]: e.target.value
		})
	}

	handleSubmit = (e) =>{
		console.log(this.state);
		this.props.Login(this.state)
	}
	render(){
		const { isAuth } = this.props;
		if (isAuth) return <Redirect to="/"/>
		
		return (
			<div className="container">
				<div className="box">
				<div className="row left-align">
					<h2>Login</h2>
				</div>
					<form className="col s12">
						<div className="row">
							<div className="input-field col l5 m5 s12">
								<input placeholder="" id="username" type="text" className="validate" onChange={ this.handleChange } />
								<label forhtml="username">Username</label>
							</div>
						</div>
						<div className="row">
							<div className="input-field col l5 m5 s12">
								<input placeholder="" id="password" type="password" className="validate" onChange={ this.handleChange } />
								<label forhtml="password">Password</label>
							</div>
						</div>
						<div className="row">
							<button className="btn waves-effect waves-light" type="button" onClick={ this.handleSubmit } name="action">Login
    							<i className="material-icons right">send</i>
  							</button>
						</div>
					</form>
				</div>
			</div>
		)
	}
}

const mapStateToProps = (state) => {
	return {
		isAuth: state.auth.isAuth
	};
}

const mapDispatchToProps = (dispatch) => {
	return {
		Login: (user) => dispatch(login(user))
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Signin);