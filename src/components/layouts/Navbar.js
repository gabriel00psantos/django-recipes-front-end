import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { logOut } from '../../store/actions/authActions';
import SignInLinks  from './SignInLinks';
import SingOutLinks from './SignOutLinks';


const Navbar = (props) => {
	let { isAuth } = props;
	let links = isAuth ? <SignInLinks logout={props.logout}/> : <SingOutLinks/>; 
	return (
		<React.Fragment>
		<nav>
			<div className="nav-wrapper">
			<Link to="/" className="brand-logo"><span>&#10037;</span></Link>
				<ul id="nav-mobile" className="right hide-on-med-and-down">
					<li><Link to="/">Dashboard</Link></li>
					{ links }
				</ul>
			</div>
		</nav>
		<ul class="sidenav" id="mobile-demo">
			<li><a href="sass.html">Sass</a></li>
			<li><a href="badges.html">Components</a></li>
			<li><a href="collapsible.html">Javascript</a></li>
			<li><a href="mobile.html">Mobile</a></li>
		</ul>
		</React.Fragment>
	)
};

const mapStateToProps = (state) => {
	return {
		isAuth: state.auth.isAuth
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
		logout: () => dispatch(logOut())
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Navbar);