import React from 'react';
import { Link } from 'react-router-dom';

const SignInLinks = (props) => {
	return (
			<React.Fragment>
				<li><Link to="/create">Add recipes</Link></li>
				<li><a href="/" onClick={ props.logout }>Logout</a></li>
			</React.Fragment>
	)
}

export default SignInLinks;