import React from 'react';

const Loader = ({ active }) => {
	const status = active ? 'loader show' : 'loader hide';
	return(
	<div className={ status }>
		<div className="preloader-wrapper big active loader-center">
			<div className="spinner-layer spinner-blue-only">
				<div className="circle-clipper left">
					<div className="circle"></div>
				</div>
			<div className="gap-patch">
				<div className="circle"></div>
			</div>
			<div className="circle-clipper right">
				<div className="circle"></div>
			</div>
			</div>
		</div>
	</div>
	)
}

export default Loader;