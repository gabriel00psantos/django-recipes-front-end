import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import { AnimatedSwitch } from 'react-router-transition';
import './App.css';
import "toasted-notes/src/styles.css";
import Navbar from './components/layouts/Navbar';
import Dashboard from './components/dashboard/Dashboard';
import CreateRecipe from './components/recipes/CreateRecipe';
import Signin from './components/auth/SignIn';
import Signup from './components/auth/Signup';
import RecipeDetails from './components/recipes/RecipeDetails';


function App() {
  return (
	    <BrowserRouter>
	    	<div className="App">
		        <Navbar/>
		        <AnimatedSwitch
			      atEnter={{ opacity: 0 }}
			      atLeave={{ opacity: 0 }}
			      atActive={{ opacity: 1 }}
			      className="switch-wrapper"
    			>
		        	<Route exact path="/details/:id" component={ RecipeDetails }/>
		        	<Route exact path="/" component={ Dashboard }/>
		        	<Route exact path="/create" component={ CreateRecipe }/>
		        	<Route exact path="/signin" component={ Signin }/>
		        	<Route exact path="/signup" component={ Signup }/>
		        </AnimatedSwitch>
		    </div>
		</BrowserRouter>
  );
}

export default App;
